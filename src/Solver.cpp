#include "Solver.h"
#include <iostream>
#include <initializer_list>
#include <algorithm>
#include <iterator>
#include <limits>
#include <cstdlib>

static const std::initializer_list<Distribution> DISTRIBUTIONS{GAUSS, POISSON, UNIFORM, EXPONENTIAL};

double Solver::get_distribution_kurtosis(const Distribution distribution, const double mean) {
	switch (distribution) {
		case EXPONENTIAL: return 6.0;
		case UNIFORM: return -(6.0 / 5.0);
		case POISSON: return (1.0 / mean);  
		case GAUSS: return 0.0;
		default: return -1.0; 
	}
}

std::string Solver::get_distribution(Running_Stats stats)
{
	std::string output;
	
	Distribution distribution{GAUSS};
	auto min = std::numeric_limits<double>::max();

	for (const auto a : DISTRIBUTIONS) {
		const auto distribution_kurtosis = get_distribution_kurtosis(a, mean(stats));
		const auto deviation = abs(abs(distribution_kurtosis) - abs(kurtosis(stats)));
		if (deviation < min) {
			min = deviation;
			distribution = a;
		}
	}

	
	std::cout << "\nResult: ";
	switch (distribution)
	{
	case GAUSS:
		output = "Gauss";
		break;
	case POISSON:
		output = "Poisson";
		break;
	case UNIFORM:
		output = "Uniform";
		break;
	case EXPONENTIAL:
		output = "Exponential";
		break;
	}

	return output;
}
