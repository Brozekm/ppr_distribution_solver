#include <vector>
#include <string>
#include "RunningStats.h"
#include "Solver.h"

#pragma once
class Sequential_Solver : public Solver
{
public:
	void analyze_file(const std::string& filename);
	Running_Stats get_stats();

private:
	void analyze_data(std::vector<double>& data);
	Running_Stats stats{ 0, 0, 0, 0, 0 };
};

