#include <vector>
#include <string>
#include "RunningStats.h"

enum Distribution
{
	GAUSS,
	POISSON,
	UNIFORM,
	EXPONENTIAL
};

#pragma once
class Solver {
public:
	static const unsigned int NUMBER_IN_CHUNK = 256;
	static const unsigned int FILE_CHUNK_SIZE = 500 * NUMBER_IN_CHUNK;
	static const unsigned int CHUNK_SIZE_BYTES = sizeof(double) * FILE_CHUNK_SIZE;

	virtual void analyze_file(const std::string& filename) = 0;
	virtual Running_Stats get_stats() = 0;

	std::string get_distribution(Running_Stats stats);
private:
	double get_distribution_kurtosis(const Distribution distribution, const double mean);
};
