#include <iostream>
#include <string>
#include <vector>
#include <chrono>
#include <fstream>
#include <unordered_map>
#include "SMP_Parallel_Solver.h"
#include "Sequential_Solver.h"
#include "Solver.h"


void exit_app() {
	std::string line;
	std::cout << "Press any key to exit" << std::endl;
	std::getline(std::cin, line);
	exit(1);
}

enum LaunchOptions
{
	ALL,
	SMP,
	GPU,
	SEQ
};

static std::unordered_map<std::string, LaunchOptions> const LAUNCH_OPTIONS_MAPPING = { {"all", LaunchOptions::ALL}, {"smp", LaunchOptions::SMP},
	{"gpu", LaunchOptions::GPU}, {"seq", LaunchOptions::SEQ}};


std::unique_ptr<Solver> get_solver(std::string& solver) {
	std::transform(solver.begin(), solver.end(), solver.begin(), ::tolower);
	const auto it = LAUNCH_OPTIONS_MAPPING.find(solver);
	if (it == LAUNCH_OPTIONS_MAPPING.end())
	{
		std::cout << solver << " is not an option\n";
		std::cout << "Launch options: 'seq' 'smp'" << std::endl;
		return nullptr;
	}

	std::cout << "Executed: " << it->first << std::endl;

	switch (it->second)
	{
	case SMP:
		return std::make_unique<SMP_Parallel_Solver>();
	case SEQ: 
		return std::make_unique<Sequential_Solver>();
	case GPU:
	case ALL: 
		std::cout << "Not implemented" << std::endl;
		break;
	default:
		std::cout << solver << " is not an option" << std::endl;
		break;
	}

	return nullptr;
}

std::ifstream::pos_type filesize(const char* filename)
{
	std::ifstream in(filename, std::ifstream::ate | std::ifstream::binary);
	if (!in) {
		std::string line;
		std::cout << "File (" << filename << ") could not be opend" << std::endl;
		exit_app();
	}

	return in.tellg();
}
/*
std::unique_ptr<Solver> get_distribution(int a) {
	if (a == 1) {
		return std::make_unique<SMP_Parallel_Solver>();
	}
	else {
		return std::make_unique<Sequential_Solver>();
	}
}
*/



int main(int argc, char** argv) {
	if (argc != 3) {
		std::cout << "Wrong number of arguments\n";
		std::cout << "Expected format: './<app.exe> <input_file> <launch_option>'\n";
		std::cout << "Launch options: 'seq' 'smp'" << std::endl;
		exit_app();
	}
		

	std::string line;
	const std::string input_file_name = std::string(argv[1]);
	filesize(input_file_name.c_str());
	//const std::string input_file_name = "C:/Users/broze/School/ppr_distribution_solver/referencni_rozdeleni/gauss";
	
	std::string run_option = std::string(argv[2]);
	std::unique_ptr<Solver> solver = get_solver(run_option);

	if (!solver)
	{
		exit_app();
	}
	
	const auto start = std::chrono::high_resolution_clock::now();

	solver->analyze_file(input_file_name);
	const auto a = solver->get_stats();

	const auto end = std::chrono::high_resolution_clock::now();
	const auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	std::cout << "Time: " << duration.count() << "ms \n" << std::endl;

	print_stats(a);
		std::cout << solver->get_distribution(a) << "\n" << std::endl;
	
	
	exit_app();
}