#include "RunningStats.h"
#include <cmath>
#include <iostream>
#include <future>
#include <execution>
#include <numeric>

double mean(const Running_Stats& stats)
{
	return stats.M1;
}

double variance(const Running_Stats& stats)
{
	return stats.M2 / (stats.n - 1.0);
}

double standard_deviation(const Running_Stats& stats)
{
	return sqrt(variance(stats));
}

double skewness(const Running_Stats& stats)
{
	return sqrt(double(stats.n)) * stats.M3 / pow(stats.M2, 1.5);
}

double kurtosis(const Running_Stats& stats)
{
	return double(stats.n) * stats.M4 / (stats.M2 * stats.M2) - 3.0;
}

void print_stats(const Running_Stats& stats)
{
	std::cout << "Analyzed data:\n";
	std::cout << "	Data count (n): " << (unsigned int) stats.n << "\n";
	std::cout << "	Mean: " << mean(stats) << "\n";
	std::cout << "	Variance: " << variance(stats) << "\n";
	std::cout << "	Standard deviation: " << standard_deviation(stats) << "\n";
	std::cout << "	Skewness: " << skewness(stats) << "\n";
	std::cout << "	Kurtosis: " << kurtosis(stats) << "\n";
}

Running_Stats merge_running_stats(const Running_Stats& a, const Running_Stats& b)
{
	Running_Stats combined;

	combined.n = a.n + b.n;

	double delta = b.M1 - a.M1;
	double delta2 = delta * delta;
	double delta3 = delta * delta2;
	double delta4 = delta2 * delta2;

	combined.M1 = (a.n * a.M1 + b.n * b.M1) / combined.n;

	combined.M2 = a.M2 + b.M2 +
		delta2 * a.n * b.n / combined.n;

	combined.M3 = a.M3 + b.M3 +
		delta3 * a.n * b.n * (a.n - b.n) / (combined.n * combined.n);
	combined.M3 += 3.0 * delta * (a.n * b.M2 - b.n * a.M2) / combined.n;

	combined.M4 = a.M4 + b.M4 + delta4 * a.n * b.n * (a.n * a.n - a.n * b.n + b.n * b.n) /
		(combined.n * combined.n * combined.n);
	combined.M4 += 6.0 * delta2 * (a.n * a.n * b.M2 + b.n * b.n * a.M2) / (combined.n * combined.n) +
		4.0 * delta * (a.n * b.M3 - b.n * a.M3) / combined.n;

	return combined;
}

Running_Stats merge_future_stats(std::vector<std::future<Running_Stats>>& future_results) {
	std::vector<Running_Stats> results;
	results.reserve(future_results.size());

	for (unsigned int i = 0; i < future_results.size(); i++) {
		results.push_back(future_results.at(i).get());
	}
	future_results.clear();
	return std::reduce(std::execution::par, results.cbegin(), results.cend());
}


Running_Stats operator+(const Running_Stats a, const Running_Stats b) {
	Running_Stats combined;

	combined.n = a.n + b.n;

	double delta = b.M1 - a.M1;
	double delta2 = delta * delta;
	double delta3 = delta * delta2;
	double delta4 = delta2 * delta2;

	combined.M1 = (a.n * a.M1 + b.n * b.M1) / combined.n;

	combined.M2 = a.M2 + b.M2 +
		delta2 * a.n * b.n / combined.n;

	combined.M3 = a.M3 + b.M3 +
		delta3 * a.n * b.n * (a.n - b.n) / (combined.n * combined.n);
	combined.M3 += 3.0 * delta * (a.n * b.M2 - b.n * a.M2) / combined.n;

	combined.M4 = a.M4 + b.M4 + delta4 * a.n * b.n * (a.n * a.n - a.n * b.n + b.n * b.n) /
		(combined.n * combined.n * combined.n);
	combined.M4 += 6.0 * delta2 * (a.n * a.n * b.M2 + b.n * b.n * a.M2) / (combined.n * combined.n) +
		4.0 * delta * (a.n * b.M3 - b.n * a.M3) / combined.n;

	return combined;
}

