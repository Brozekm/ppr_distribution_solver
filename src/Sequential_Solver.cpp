#include <fstream>
#include <iostream>
#include "Sequential_Solver.h"


void Sequential_Solver::analyze_data(std::vector<double>& data)
{
    for (auto value : data) {
        double delta, delta_n, delta_n2, term1, n1;

        n1 = stats.n;
        stats.n++;
        delta = value - stats.M1;
        delta_n = delta / stats.n;
        delta_n2 = delta_n * delta_n;
        term1 = delta * delta_n * n1;
        stats.M1 += delta_n;
        stats.M4 += term1 * delta_n2 * (stats.n * stats.n - 3 * stats.n + 3) + 6 * delta_n2 * stats.M2 - 4 * delta_n * stats.M3;
        stats.M3 += term1 * delta_n * (stats.n - 2) - 3 * delta_n * stats.M2;
        stats.M2 += term1;
    }
}

void Sequential_Solver::analyze_file(const std::string& filename)
{
	std::ifstream input_file(filename, std::ifstream::in | std::ifstream::binary);

	if (!input_file) {
		std::cout << "Input file (" << filename << ") was not opend succesfully" << std::endl;
		return;
	}


	unsigned int chunk_size = sizeof(double) * Solver::FILE_CHUNK_SIZE;

	std::vector<double> buffer(Solver::FILE_CHUNK_SIZE);

	while (!input_file.eof()) {
		input_file.read(reinterpret_cast<char*>(buffer.data()), chunk_size);

		if (input_file.gcount() == 0) {
			break;
		}

		if (input_file.gcount() < CHUNK_SIZE_BYTES) {
			const auto trimmed_buffer_size = (unsigned int)input_file.gcount() / sizeof(double);
			buffer.resize(trimmed_buffer_size);
		}

		std::vector<double> valid_data;
		valid_data.reserve(buffer.size());
		for (const auto number : buffer) {
			const auto number_class = fpclassify(number);
			if (number_class == FP_NORMAL || number_class == FP_ZERO) {
				valid_data.push_back(number);
			}
		}


		analyze_data(valid_data);

	}

}

Running_Stats Sequential_Solver::get_stats()
{
    return stats;
}
