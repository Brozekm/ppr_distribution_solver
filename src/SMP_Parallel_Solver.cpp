#include "SMP_Parallel_Solver.h"
#include <iostream>
#include <array>
#include <fstream>
#include <future>
#include <algorithm>
#include <execution>
#include <execution>
#include <numeric>
#include "RunningStats.h"


/*
Running_Stats analyze_data_smaller(std::vector<double> data, unsigned int from) {
	std::array<double, Solver::NUMBER_IN_CHUNK> n{ 0 };
	std::array<double, Solver::NUMBER_IN_CHUNK> M1{ 0 };
	std::array<double, Solver::NUMBER_IN_CHUNK> M2{ 0 };
	std::array<double, Solver::NUMBER_IN_CHUNK> M3{ 0 };
	std::array<double, Solver::NUMBER_IN_CHUNK> M4{ 0 };


	for (unsigned int i = 0; i < Solver::NUMBER_IN_CHUNK; i++) {
		const auto n1 = n[i];
		const auto n2 = n[i] + 1;

		const double delta = data[from + i] - M1[i];
		const double delta_n = delta / n2;
		const double delta_n2 = delta_n * delta_n;
		const double term1 = delta * delta_n * n1;
		M1[i] += delta_n;

		M4[i] += term1 * delta_n2 * (n2 * n2 - 3 * n2 + 3) + 6 * delta_n2 * M2[i] - 4 * delta_n * M3[i];
		M3[i] += term1 * delta_n * (n2 - 2) - 3 * delta_n * M2[i];
		M2[i] += term1;

		n[i] = n2;
	}


	std::vector<Running_Stats> results;
	results.reserve(Solver::NUMBER_IN_CHUNK);
	for (unsigned int i = 0; i < n.size(); i++) {
		Running_Stats s = { n[i],M1[i], M2[i], M3[i], M4[i] };
		results.push_back(s);
	}

	return std::reduce(std::execution::par, results.cbegin(), results.cend());
}


Running_Stats analyze_data(std::vector<double> data)
{
	if (data.size() != Solver::FILE_CHUNK_SIZE)
	{
		return Running_Stats();
	}


	std::vector<std::future<Running_Stats>> future_results;
	future_results.reserve(500);

	for (unsigned int j = 0; j < data.size(); j += Solver::NUMBER_IN_CHUNK) {
		future_results.push_back(std::async(std::launch::async, analyze_data_smaller, data, j));
	}

	std::vector<Running_Stats> results;
	results.reserve(500);

	for (unsigned int i = 0; i < future_results.size(); i++) {
		results.push_back(future_results.at(i).get());
	}

	return std::reduce(std::execution::par, results.cbegin(), results.cend());
}*/


Running_Stats analyze_data(std::vector<double> data)
{
	//const auto start = std::chrono::high_resolution_clock::now();
	/*if ((data.size() % Solver::NUMBER_IN_CHUNK) != 0)
	{
		std::cout << "Data size (" << data.size() << ") must be divisible by " << Solver::NUMBER_IN_CHUNK << std::endl;
		return Running_Stats();
	}*/
	std::vector<double> valid_data;
	valid_data.reserve(data.size());
	for (const auto number : data) {
		const auto number_class = fpclassify(number);
		if (number_class == FP_NORMAL || number_class == FP_ZERO) {
			valid_data.push_back(number);
		}
	}

	if (valid_data.size() < Solver::NUMBER_IN_CHUNK) {
		return Running_Stats();
	}
	
	std::array<double, Solver::NUMBER_IN_CHUNK> n{ 0 };
	std::array<double, Solver::NUMBER_IN_CHUNK> M1{ 0 };
	std::array<double, Solver::NUMBER_IN_CHUNK> M2{ 0 };
	std::array<double, Solver::NUMBER_IN_CHUNK> M3{ 0 };
	std::array<double, Solver::NUMBER_IN_CHUNK> M4{ 0 };

	
	for(unsigned int j = 0; j < valid_data.size()-Solver::NUMBER_IN_CHUNK; j+= Solver::NUMBER_IN_CHUNK){
		for (unsigned int i = 0; i < Solver::NUMBER_IN_CHUNK; i++) {
			const auto n1 = n[i];
			const auto n2 = n[i] + 1;

			const double delta = valid_data[j+i] - M1[i];
			const double delta_n = delta / n2;
			const double delta_n2 = delta_n * delta_n;
			const double term1 = delta * delta_n * n1;
			M1[i] += delta_n;

			M4[i] += term1 * delta_n2 * (n2 * n2 - 3 * n2 + 3) + 6 * delta_n2 * M2[i] - 4 * delta_n * M3[i];
			M3[i] += term1 * delta_n * (n2 - 2) - 3 * delta_n * M2[i];
			M2[i] += term1;

			n[i] = n2;
		}
	}
	
	std::vector<Running_Stats> results;
	results.reserve(Solver::NUMBER_IN_CHUNK);
	for (unsigned int i = 0; i < Solver::NUMBER_IN_CHUNK; i++) {
		Running_Stats s = {n[i],M1[i], M2[i], M3[i], M4[i]};
		results.push_back(s);
	}
	//const auto result = std::reduce(std::execution::par, results.cbegin(), results.cend());

	//const auto end = std::chrono::high_resolution_clock::now();
	//const auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
	//std::cout << "Analysing in micro: " << duration.count() << std::endl;
	return std::reduce(std::execution::par, results.cbegin(), results.cend());
}




void SMP_Parallel_Solver::analyze_file(const std::string& filename)
{
	std::ifstream input_file(filename, std::ifstream::in | std::ifstream::binary);

	if (!input_file) {
		std::cout << "Input file (" << filename << ") was not opend succesfully" << std::endl;
		return;
	}

	unsigned int chunk_size = sizeof(double) * Solver::FILE_CHUNK_SIZE;

	std::vector<double> buffer(Solver::FILE_CHUNK_SIZE);

	
	std::vector<std::future<Running_Stats>> future_results;
	future_results.reserve(ASYNC_STATS_CAP);


	while (!input_file.eof()) {
		input_file.read(reinterpret_cast<char*>(buffer.data()), CHUNK_SIZE_BYTES);

		if (input_file.gcount() == 0) {
			break;
		}

		if (input_file.gcount() < CHUNK_SIZE_BYTES) {
			const auto trimmed_buffer_size = (unsigned int)input_file.gcount() / sizeof(double);
			buffer.resize(trimmed_buffer_size);
		}
		
		//std::cout << buffer.size() << std::endl;
		

		future_results.push_back(std::async(std::launch::async, analyze_data, buffer));
		
		if (future_results.size() == ASYNC_STATS_CAP) {
			result = result + merge_future_stats(future_results);
		}	
	}

	if (!future_results.empty()) {
		result = result + merge_future_stats(future_results);
	}
	
}

Running_Stats SMP_Parallel_Solver::get_stats()
{
	return result;
}


/*
Running_Stats get_stats_sequentially(
	std::array<double, Solver::NUMBER_IN_CHUNK>& n,
	std::array<double, Solver::NUMBER_IN_CHUNK>& M1,
	std::array<double, Solver::NUMBER_IN_CHUNK>& M2,
	std::array<double, Solver::NUMBER_IN_CHUNK>& M3,
	std::array<double, Solver::NUMBER_IN_CHUNK>& M4
)
{
	std::vector<Running_Stats> merged_results;
	merged_results.reserve(Solver::NUMBER_IN_CHUNK);

	for (unsigned int i = 0; i < Solver::NUMBER_IN_CHUNK; i++) {
		Running_Stats calculation_a = { n[i], M1[i], M2[i], M3[i], M4[i] };
		merged_results.push_back(calculation_a);
	}

	while (merged_results.size() >= 2) {
		const auto calculation_a = merged_results.front();
		merged_results.erase(merged_results.begin());
		const auto calculation_b = merged_results.front();
		merged_results.erase(merged_results.begin());
		const auto caultulation_new = merge_running_stats(calculation_a, calculation_b);
		merged_results.push_back(caultulation_new);
	}
	return merged_results[0];
}
*/