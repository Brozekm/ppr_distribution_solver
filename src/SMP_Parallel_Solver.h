#include <vector>
#include <array>
#include "RunningStats.h"
#include "Solver.h"


#pragma once
class SMP_Parallel_Solver : public Solver
{
public:
	static const int NUMBER_IN_CHUNK = 256;
	static const int ASYNC_STATS_CAP = 1000;

	void analyze_file(const std::string& filename);
	Running_Stats get_stats();
	/*Running_Stats get_stats_sequentially(std::array<double, NUMBER_IN_CHUNK>& n,
		std::array<double, NUMBER_IN_CHUNK>& M1,
		std::array<double, NUMBER_IN_CHUNK>& M2,
		std::array<double, NUMBER_IN_CHUNK>& M3,
		std::array<double,NUMBER_IN_CHUNK>& M4);*/
	//Running_Stats get_stats_parallelly();


private:
	Running_Stats result{0,0,0,0,0};
	//Running_Stats analyze_data(std::vector<double> data);
	std::vector<Running_Stats> stats;
};

