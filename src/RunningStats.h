#include <vector>
#include <future>


#pragma once
struct Running_Stats {
    double n, M1, M2, M3, M4;
};

double mean(const Running_Stats& stats);
double variance(const Running_Stats& stats);
double standard_deviation(const Running_Stats& stats);
double skewness(const Running_Stats& stats);
double kurtosis(const Running_Stats& stats);
void print_stats(const Running_Stats& stats);

Running_Stats merge_running_stats(const Running_Stats& a, const Running_Stats& b);
Running_Stats merge_future_stats(std::vector<std::future<Running_Stats>>& future_results);
Running_Stats operator+(const Running_Stats a, const Running_Stats b);